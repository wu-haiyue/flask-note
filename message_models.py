'''
@Project ：flaskNote 
@File    ：message_models.py
@IDE     ：PyCharm 
@Author  ：why
@Date    ：2023/4/12 15:00 
'''
from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import pymysql
pymysql.install_as_MySQLdb()

app = Flask(__name__)

# 通过修改以下代码来操作不同的SQL比写原生SQL简单很多 --》通过ORM可以实现从底层更改使用的SQL,pymysql:纯 Python实现的一个驱动,因此可以和 Python代码无缝衔接。
app.config["SQLALCHEMY_DATABASE_URI"] = 'mysql+pymysql://root:123456@127.0.0.1:3306/flaskdb2'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = "flaskNote02"
#查询时会显示原始SQL语句
app.config['SQLALCHEMY_ECHO'] = True

db = SQLAlchemy(app)   # 实例化数据库

# 管理员
class Admin(db.Model):
    __tablename__ = "admin"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), nullable=False, unique=True)
    password = db.Column(db.String(64), nullable=False)
    tags = db.relationship("Tag", backref="admin")    # 关联表

# 标签
class Tag(db.Model):
    __tablename__ = "tag"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(10), nullable=False, unique=True)  # 标签名字
    admin_id = db.Column(db.Integer, db.ForeignKey("admin.id"))  # 所属管理员，只有管理员可以创建标签

# 用户
class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), nullable=False, unique=True)
    password = db.Column(db.String(64), nullable=False)
    messages = db.relationship("Message", backref="user")  # 关联表

# 留言条
class Message(db.Model):
    __tablename__ = "message"
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(256), nullable=False)
    create_time = db.Column(db.DateTime, default=datetime.now)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))  # 所属用户，用户创建留言条， 一对多
    tags = db.relationship("Tag", secondary="message_to_tag", backref="messages")  # 关系关联

# 中间表
class MessageToTag(db.Model):
    __tablename__ = "message_to_tag"
    id = db.Column(db.Integer, primary_key=True)
    message_id = db.Column(db.Integer, db.ForeignKey("message.id", ondelete='CASCADE'))  # 所属留言条   ondelete='CASCADE'，实现级联删除
    tag_id = db.Column(db.Integer, db.ForeignKey("tag.id", ondelete='CASCADE'))          # 所属标题


# if __name__ == '__main__':
#     with app.app_context():
#         db.create_all()
        # db.drop_all()
    # 创建数据库
    # db.create_all()
    # app.run(host='0.0.0.0', port=4569, debug=True)
    # app.run()
