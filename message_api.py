'''
@Project ：flaskNote 
@File    ：message_api.py
@IDE     ：PyCharm 
@Author  ：why
@Date    ：2023/4/12 15:01 
'''
from flask import jsonify, request, session

from message_models import db, app, Admin, User, Tag, Message

'''
状态码  200 成功
状态码  400 失败

管理员初始化不能这样写，让别人知道这个接口，就能初始化管理员的账号，非常危险， 目前先这样写
'''


@app.route('/')
@app.route('/index')
def index():
    return "hello world"


# 管理员初始化
@app.route('/init/admin')
def init_admin():
    admin = Admin(username="admin", password="default")
    try:
        db.session.add(admin)
        db.session.commit()
        return jsonify(code=200, msg="管理员初始化成功")
    except Exception as e:
        print(e)
        db.session.rollback()     # 数据返回来，不要在数据表里扰乱
        return jsonify(code=400, msg="管理员初始化失败")


# 管理员登录
@app.route('/admin/login', methods=["POST"])
def admin_login():
    req_data = request.get_json()
    username = req_data.get("username")
    password = req_data.get("password")
    if not all([username, password]):
        return jsonify(code=400, msg="参数不完整")
    admin = Admin.query.filter(Admin.username == username).first()
    if admin is None or password != admin.password:
        return jsonify(code=400, msg="账号或密码错误")
    session["admin_name"] = username
    session["admin_id"] = admin.id

    return jsonify(msg="登录成功")


# 检验登录状态
@app.route('/admin/session', methods=["GET"])
def check_session():
    # 获取一个就行，有一个就证明已经登录了
    username = session.get("admin_name")
    admin_id = session.get("admin_id")
    if username is not None:
        return jsonify(username=username, admin_id=admin_id)
    else:
        return jsonify(msg="出错了，没登录")

# 管理员登出
@app.route('/admin/logout')
def admin_logout():
    session.clear()
    return jsonify(msg="退出登录")

# 管理员增标签
@app.route('/admin/tag', methods=["POST"])
def add_tag():
    req_data = request.get_json()
    tag_name = req_data.get("tag_name")
    admin_id = session.get("admin_id")
    if not all([tag_name, admin_id]):
        return jsonify(code=400, msg="参数不完整或者未登录")
    tag = Tag(name=tag_name, admin_id=admin_id)
    try:
        db.session.add(tag)
        db.session.commit()
        return jsonify(code=200, msg="添加成功")
    except Exception as e:
        print(e)
        db.session.rollback()
        return jsonify(code=400, msg="添加失败")


# 管理员删除标签
@app.route('/admin/tag', methods=["DELETE"])
def del_tag():
    req_data = request.get_json()
    tag_name = req_data.get("tag_name")
    admin_id = session.get("admin_id")
    if not all([tag_name, admin_id]):
        return jsonify(code=400, msg="参数不完整或者未登录")
    try:
        tag = Tag.query.filter(Tag.name==tag_name).delete()
        db.session.commit()
        return jsonify(code=200, msg="删除成功")
    except Exception as e:
        print(e)
        db.session.rollback()
        return jsonify(code=400, msg="删除失败")

# 管理员删除留言
@app.route('/admin/message', methods=["DELETE"])
def adm_del_message():
    req_data = request.get_json()
    message_id = req_data.get("message_id")
    user_id = session.get("user_id")
    if not all({message_id, user_id}):
        return jsonify(code=400, msg="参数不完整或者未登录")
    # 判断留言是否存在
    msg = Message.query.get(message_id)
    if msg is None:
        return jsonify(code=400, msg="留言不存在")
    try:
        m = Message.query.filter(Message.id == message_id).delete()
        db.session.commit()
        return jsonify(code=200, msg="删除成功")
    except Exception as e:
        print(e)
        db.session.rollback()
        return jsonify(code=400, msg="删除失败")



# 用户注册
@app.route('/user/register', methods=["POST"])
def add_user():
    req_data = request.get_json()
    username = req_data.get("username")
    password = req_data.get("password")
    user = User(username=username, password=password)
    try:
        db.session.add(user)
        db.session.commit()
        return jsonify(code=200, msg="用户注册成功")
    except Exception as e:
        print(e)
        db.session.rollback()  # 数据返回来，不要在数据表里扰乱
        return jsonify(code=400, msg="用户注册失败")


# 用户登录
@app.route('/user/login', methods=["POST"])
def login_user():
    req_data = request.get_json()
    username = req_data.get("username")
    password = req_data.get("password")
    if not all([username, password]):
        return jsonify(code=400, msg="参数不完整")
    user = User.query.filter(User.username==username).first()
    if user is None or password != user.password:
        return jsonify(code=400, msg="账号或密码不正确")
    session["username"]=username
    session["user_id"]=user.id

    return jsonify(code=200, msg="登录成功")

# 检查用户是否已经登录
@app.route("/user/session", methods=["GET"])
def user_session():
    username = session.get("username")
    user_id = session.get("user_id")
    if username is not None:
        return jsonify(username=username, user_id=user_id)
    else:
        return jsonify(msg="出错了，没登录")


# 用户登出
@app.route('/user/logout')
def logout_user():
    session.clear()
    return jsonify(msg="退出登录")


# 用户发布留言
@app.route('/user/message', methods=["POST"])
def user_add_message():
    req_data = request.get_json()
    user_id = session.get("user_id")
    tags = req_data.get("tags")
    content = req_data.get("content")
    if not all([user_id,tags,content]):
        return jsonify(code=400, msg="参数不完整或者用户未登录")
    try:
        # 多对多添加到数据库
        tags = Tag.query.filter(Tag.name.in_(tags)).all()
        message = Message(content = content, user_id = user_id)
        message.tags = tags
        db.session.add(message)
        db.session.commit()
        return jsonify(code=200, msg="发布留言成功")
    except Exception as e:
        print(e)
        db.session.rollback()
        return jsonify(code=400, msg="发布留言失败")

# 用户删除留言
@app.route('/user/message/del', methods=["DELETE"])
def user_del_message():
    """
    判断是否是留言的发布者  user_id
    :return:
    """
    req_data = request.get_json()
    message_id = req_data.get("message_id")
    user_id = session.get("user_id")
    if not all({message_id, user_id}):
        return jsonify(code=400, msg="参数不完整或者未登录")

    # 判断留言是否存在
    msg = Message.query.get(message_id)
    if msg is None:
        return jsonify(code=400, msg="留言不存在")
    # 判断用户是否是作者
    if user_id != msg.user.id:
        return jsonify(code=400, msg="你不是作者，无法删除")
    try:
        m = Message.query.filter(Message.id == message_id).delete()
        db.session.commit()
        return jsonify(code=200, msg="删除成功")
    except Exception as e:
        print(e)
        db.session.rollback()
        return jsonify(code=400, msg="删除失败")


# 用户查看自己的所有留言
@app.route('/user/message/history')
def user_m_history():
    user_id = session.get("user_id")
    if user_id is None:
        return jsonify(code=400, msg="请登录")
    user = User.query.get(user_id)
    if user is None:
        return jsonify(code=400, msg="用户不存在")
    # 获取留言
    paylod = []
    messages = user.messages
    for message in messages:
        content = message.content,
        # 获取tags
        tag_names = []
        tags = message.tags,
        for tag in tags:
            for t in tag:
                print(t.name)
                tag_names.append(t.name)
        create_time = message.create_time.strftime("%Y-%m-%d %H:%M:%S"),
        user_id = message.user_id
        data = {"message_id": message.id,
                "tags": tag_names,
                "content": content,
                "create_time": create_time,
                "user_id": user_id}
        paylod.append(data)

    return jsonify(code=200, msg="查询成功", data=paylod)


# 所有人都能查看的留言板
@app.route('/message/board')
def msg_board():
    # 获取留言
    messages = Message.query.all()
    payload = []
    for message in messages:
        content = message.content,
        # 获取tags
        tag_names = []
        tags = message.tags,
        for tag in tags:
            for t in tag:
                print(t.name)
                tag_names.append(t.name)
        create_time = message.create_time.strftime("%Y-%m-%d %H:%M:%S"),
        user_id = message.user_id
        data = {"message_id": message.id,
                "tags": tag_names,
                "content": content,
                "create_time": create_time,
                "user_id": user_id}
        payload.append(data)

    return jsonify(code=200, msg="查询成功", data=payload)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5050, debug=True)